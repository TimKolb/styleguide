const override = require('./config-overrides.js');

const originalWebpackConfig =
    process.env.NODE_ENV !== 'production'
        ? require('react-scripts-ts/config/webpack.config.dev.js')
        : require('react-scripts-ts/config/webpack.config.prod.js');
const webpackConfig = override(originalWebpackConfig, process.env.NODE_ENV);

module.exports = {
    components: 'src/{components,groups}/**/*.{ts,tsx}',
    propsParser: require('react-docgen-typescript').parse,
    webpackConfig
};
