import * as React from 'react';
import { Component, ReactNode } from 'react';
import { Button, Tooltip } from 'antd';
import { css } from 'emotion';
import { Card } from '../components/Card';
import { LinkToAnt } from '../components/LinkToAnt';
import { CodeExpand } from '../composite/CodeExpand';

interface ToolTipProps {
    buttonWidth?: number;
    text?: ReactNode;
}

const CenterHorizontal = css`
    display: flex;
    justify-content: center;
`;

export class ToolTips extends Component<ToolTipProps> {
    static defaultProps: Partial<ToolTipProps> = {
        buttonWidth: 70,
        text: <span>prompt text</span>,
    };

    render() {
        const { text, buttonWidth } = this.props;
        const buttonStyle = css`
            width: ${buttonWidth}px;
        `;

        return (
            <CodeExpand code={require(`!raw-loader!${__filename}`)}>
                {(_, { renderToggle, renderCode }) => (
                    <Card
                        title="Tooltips"
                        extra={
                            <>
                                <LinkToAnt componentName="tooltip" />
                                {renderToggle()}
                            </>
                        }
                    >
                        <div className={CenterHorizontal}>
                            <div>
                                <div style={{ marginLeft: buttonWidth, whiteSpace: 'nowrap' }}>
                                    <Tooltip placement="topLeft" title={text}>
                                        <Button className={buttonStyle}>TL</Button>
                                    </Tooltip>
                                    <Tooltip placement="top" title={text}>
                                        <Button className={buttonStyle}>Top</Button>
                                    </Tooltip>
                                    <Tooltip placement="topRight" title={text}>
                                        <Button className={buttonStyle}>TR</Button>
                                    </Tooltip>
                                </div>
                                <div style={{ width: buttonWidth, float: 'left' }}>
                                    <Tooltip placement="leftTop" title={text}>
                                        <Button className={buttonStyle}>LT</Button>
                                    </Tooltip>
                                    <Tooltip placement="left" title={text}>
                                        <Button className={buttonStyle}>Left</Button>
                                    </Tooltip>
                                    <Tooltip placement="leftBottom" title={text}>
                                        <Button className={buttonStyle}>LB</Button>
                                    </Tooltip>
                                </div>
                                <div style={{ width: buttonWidth, marginLeft: buttonWidth! * 4 }}>
                                    <Tooltip placement="rightTop" title={text}>
                                        <Button className={buttonStyle}>RT</Button>
                                    </Tooltip>
                                    <Tooltip placement="right" title={text}>
                                        <Button className={buttonStyle}>Right</Button>
                                    </Tooltip>
                                    <Tooltip placement="rightBottom" title={text}>
                                        <Button className={buttonStyle}>RB</Button>
                                    </Tooltip>
                                </div>
                                <div
                                    style={{
                                        marginLeft: buttonWidth,
                                        clear: 'both',
                                        whiteSpace: 'nowrap',
                                    }}
                                >
                                    <Tooltip placement="bottomLeft" title={text}>
                                        <Button className={buttonStyle}>BL</Button>
                                    </Tooltip>
                                    <Tooltip placement="bottom" title={text}>
                                        <Button className={buttonStyle}>Bottom</Button>
                                    </Tooltip>
                                    <Tooltip placement="bottomRight" title={text}>
                                        <Button className={buttonStyle}>BR</Button>
                                    </Tooltip>
                                </div>
                            </div>
                        </div>
                        {renderCode()}
                    </Card>
                )}
            </CodeExpand>
        );
    }
}
