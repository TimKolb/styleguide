import * as React from 'react';
import { Component } from 'react';
import { Radio, Switch, Tag, Row, Col } from 'antd';
import { ComponentRow } from '../components/ComponentRow';
import { Card } from '../components/Card';
import { RadioChangeEvent } from 'antd/lib/radio';
import { LinkToAnt } from '../components/LinkToAnt';

import 'highlight.js/styles/vs.css';
import { CodeExpand } from '../composite/CodeExpand';
import { css } from 'emotion';
import { paddingMd } from '../theme';

type State = {
    size: 'small' | 'default';
};

export class AlternateInputs extends Component<{}, State> {
    state = {
        size: 'default' as 'small' | 'default',
    };

    handleSizeChange = (e: RadioChangeEvent) => {
        this.setState({ size: e.target.value });
    };

    public render() {
        const { size } = this.state;
        return (
            <CodeExpand code={require(`!raw-loader!${__filename}`)}>
                {(_, { renderToggle, renderCode }) => (
                    <Card
                        title="Alternate Inputs"
                        extra={
                            <>
                                <Radio.Group value={size} onChange={this.handleSizeChange}>
                                    <Radio.Button value="default">Default</Radio.Button>
                                    <Radio.Button value="small">Small</Radio.Button>
                                </Radio.Group>
                                <LinkToAnt componentName="switch" />
                                {renderToggle()}
                            </>
                        }
                    >
                        <ComponentRow>
                            <Switch size={size} defaultChecked />
                            <Switch size={size} disabled defaultChecked />
                            <Switch size={size} disabled />
                            <Switch size={size} loading defaultChecked />
                        </ComponentRow>

                        <Row
                            className={css`
                                padding-top: ${paddingMd};
                            `}
                        >
                            <Col>
                                <ComponentRow>
                                    <Tag color="magenta">magenta</Tag>
                                    <Tag color="red">red</Tag>
                                    <Tag color="volcano">volcano</Tag>
                                    <Tag color="orange">orange</Tag>
                                    <Tag color="gold">gold</Tag>
                                    <Tag color="lime">lime</Tag>
                                    <Tag color="green">green</Tag>
                                    <Tag color="cyan">cyan</Tag>
                                    <Tag color="blue">blue</Tag>
                                    <Tag color="geekblue">geekblue</Tag>
                                    <Tag color="purple">purple</Tag>
                                </ComponentRow>
                            </Col>
                        </Row>
                        {renderCode()}
                    </Card>
                )}
            </CodeExpand>
        );
    }
}
