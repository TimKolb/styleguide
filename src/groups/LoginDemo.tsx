import * as React from 'react';
import { Component } from 'react';
import { Login } from '../composite/Login';
import { Card } from '../components/Card';
import { Button } from 'antd';
import { fontFamilyFancy } from '../theme/customVariables';
import styled from 'react-emotion';
import { LoginValues } from '../composite/Login.types';
import { sleep } from '../helpers/sleep';
import { CodeExpand } from '../composite/CodeExpand';

const StyledButton = styled(Button)`
    font-family: ${fontFamilyFancy};
`;

export class LoginDemo extends Component {
    handleLogin = async (values: LoginValues) => {
        await sleep(1000);
        console.log(values);
    };

    getPropsFor = (name: string) => () => ({
        onMouseEnter: () => console.log(`MouseEnter on ${name}`),
    });

    render() {
        return (
            <CodeExpand code={require(`!raw-loader!${__filename}`)}>
                {(_, { renderToggle, renderCode }) => (
                    <Card title="Login" extra={renderToggle()}>
                        <Login
                            onSubmit={this.handleLogin}
                            components={{ Button: StyledButton }}
                            buttonChildren="Fancy Login"
                            getPropsForButton={this.getPropsFor('Button')}
                            getPropsForEmailFormItem={this.getPropsFor('EmailFormItem')}
                            getPropsForEmailInput={this.getPropsFor('EmailInput')}
                            getPropsForForm={this.getPropsFor('Form')}
                            getPropsForPasswordFormItem={this.getPropsFor('PasswordFormItem')}
                            getPropsForPasswordInput={this.getPropsFor('PasswordInput')}
                            errorLabels={{
                                email: {
                                    invalid: <span>Not valid, dude</span>,
                                    required: <span>It is very important to enter an email address...</span>,
                                },
                                password: {
                                    required: 'No password, no entry'
                                }
                            }}
                        />
                        {renderCode()}
                    </Card>
                )}
            </CodeExpand>
        );
    }
}
