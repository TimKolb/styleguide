import * as React from 'react';
import { ChangeEvent, Component } from 'react';
import { Tag, Input, Tooltip, Icon } from 'antd';

export class EditableTagGroup extends Component {
    state = {
        tags: ['Unremovable', 'Tag 2', 'Tag 3'],
        inputVisible: false,
        inputValue: '',
    };

    private input: any = null;

    handleClose = (removedTag: string) => {
        const tags = this.state.tags.filter(tag => tag !== removedTag);
        this.setState({ tags });
    };

    showInput = () => {
        this.setState({ inputVisible: true }, () => this.input.focus());
    };

    handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        this.setState({ inputValue: e.target.value });
    };

    handleInputConfirm = () => {
        const state = this.state;
        const inputValue = state.inputValue;

        let tags = state.tags;

        if (inputValue && tags.indexOf(inputValue) === -1) {
            tags = [...tags, inputValue];
        }

        this.setState({
            tags,
            inputVisible: false,
            inputValue: '',
        });
    };

    saveInputRef = (input: any) => (this.input = input);

    mapTags = (tag: string, index: number) => {
        const isLongTag = tag.length > 20;
        const tagElem = (
            <Tag key={tag} closable={index !== 0} afterClose={this.handleClose.bind(this, tag)}>
                {isLongTag ? `${tag.slice(0, 20)}...` : tag}
            </Tag>
        );
        return isLongTag ? (
            <Tooltip title={tag} key={tag}>
                {tagElem}
            </Tooltip>
        ) : (
            tagElem
        );
    };

    render() {
        const { tags, inputVisible, inputValue } = this.state;
        return (
            <div>
                {tags.map(this.mapTags)}
                {inputVisible && (
                    <Input
                        ref={this.saveInputRef}
                        type="text"
                        size="small"
                        style={{ width: 78 }}
                        value={inputValue}
                        onChange={this.handleInputChange}
                        onBlur={this.handleInputConfirm}
                        onPressEnter={this.handleInputConfirm}
                    />
                )}
                {!inputVisible && (
                    <Tag
                        onClick={this.showInput}
                        style={{ background: '#fff', borderStyle: 'dashed' }}
                    >
                        <Icon type="plus" /> New Tag
                    </Tag>
                )}
            </div>
        );
    }
}
