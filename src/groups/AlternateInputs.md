```jsx
const { Radio, Switch, Tag, Row, Col } = require('antd');
class AlternateInputs extends React.Component {
    constructor() {
        super();
        this.state = {
            size: 'default',
        };
        this.handleSizeChange = this.handleSizeChange.bind(this);
    }

    handleSizeChange(e) {
        this.setState({ size: e.target.value });
    }

    render() {
        const { size } = this.state;
        return (
            <>
                <ComponentRow>
                    <Radio.Group value={size} onChange={this.handleSizeChange}>
                        <Radio.Button value="default">Default</Radio.Button>
                        <Radio.Button value="small">Small</Radio.Button>
                    </Radio.Group>
                    <LinkToAnt componentName="switch" />
                </ComponentRow>
                <ComponentRow>
                    <Switch size={size} defaultChecked />
                    <Switch size={size} disabled defaultChecked />
                    <Switch size={size} disabled />
                    <Switch size={size} loading defaultChecked />
                </ComponentRow>

                <Row style={{ paddingTop: '1em' }}>
                    <Col>
                        <ComponentRow>
                            <Tag color="magenta">magenta</Tag>
                            <Tag color="red">red</Tag>
                            <Tag color="volcano">volcano</Tag>
                            <Tag color="orange">orange</Tag>
                            <Tag color="gold">gold</Tag>
                            <Tag color="lime">lime</Tag>
                            <Tag color="green">green</Tag>
                            <Tag color="cyan">cyan</Tag>
                            <Tag color="blue">blue</Tag>
                            <Tag color="geekblue">geekblue</Tag>
                            <Tag color="purple">purple</Tag>
                        </ComponentRow>
                    </Col>
                </Row>
            </>
        );
    }
}
<AlternateInputs />;
```
