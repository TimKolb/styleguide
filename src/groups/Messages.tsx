import * as React from 'react';
import { Component } from 'react';
import { message, Button } from 'antd';
import { css } from 'emotion';
import { paddingMd } from '../theme';
import { CodeExpand } from '../composite/CodeExpand';
import { LinkToAnt } from '../components/LinkToAnt';
import { Card } from '../components/Card';

const success = () => {
    message.success('This is a message of success');
};

const error = () => {
    message.error('This is an error');
};

const warning = () => {
    message.warning('This is a warning');
};

export const WithMarginClass = css`
    & ~ & {
        margin-left: ${paddingMd};
    }
`;

export class Messages extends Component {
    render() {
        return (
            <CodeExpand code={require(`!raw-loader!${__filename}`)}>
                {(_, { renderToggle, renderCode }) => (
                    <Card
                        title="Messages"
                        extra={
                            <>
                                <LinkToAnt componentName="message" />
                                {renderToggle()}
                            </>
                        }
                    >
                        <div>
                            <Button className={WithMarginClass} onClick={success}>
                                Success
                            </Button>
                            <Button className={WithMarginClass} onClick={error}>
                                Error
                            </Button>
                            <Button className={WithMarginClass} onClick={warning}>
                                Warning
                            </Button>
                        </div>
                        {renderCode()}
                    </Card>
                )}
            </CodeExpand>
        );
    }
}
