import * as React from 'react';
import { Component } from 'react';
import 'antd/dist/antd.less';
import { Form, Radio, Input } from 'antd';
import { RadioChangeEvent } from 'antd/lib/radio/interface';
import { ComponentRow } from '../components/ComponentRow';
import { ButtonSize } from 'antd/lib/button';
import { FormLayout } from 'antd/lib/form/Form';
import { Card } from '../components/Card';
import { css } from 'emotion';
import { paddingMd } from '../theme';
import { LinkToAnt } from '../components/LinkToAnt';
import { CodeExpand } from '../composite/CodeExpand';

const FormItem = Form.Item;

type State = {
    size: ButtonSize;
    layout: FormLayout;
};

export class Inputs extends Component<{}, State> {
    state = {
        size: 'default' as ButtonSize,
        layout: 'horizontal' as FormLayout,
    };

    handleChangeSize = (e: RadioChangeEvent) => {
        this.setState({ size: e.target.value });
    };

    handleChangeLayout = (e: RadioChangeEvent) => {
        this.setState({ layout: e.target.value });
    };

    public render() {
        const { size, layout } = this.state;
        const formItemLayout =
            layout === 'horizontal'
                ? {
                      labelCol: { span: 8 },
                      wrapperCol: { span: 16 },
                  }
                : null;

        return (
            <CodeExpand code={require(`!raw-loader!${__filename}`)}>
                {(_, { renderToggle, renderCode }) => (
                    <Card
                        title="Form"
                        extra={
                            <>
                                <Radio.Group
                                    value={size}
                                    onChange={this.handleChangeSize}
                                    className={css`
                                        margin-right: ${paddingMd};
                                    `}
                                >
                                    <Radio.Button value="large">Large</Radio.Button>
                                    <Radio.Button value="default">Default</Radio.Button>
                                    <Radio.Button value="small">Small</Radio.Button>
                                </Radio.Group>
                                <Radio.Group value={layout} onChange={this.handleChangeLayout}>
                                    <Radio.Button value="horizontal">Horizontal</Radio.Button>
                                    <Radio.Button value="inline">Inline</Radio.Button>
                                    <Radio.Button value="vertical">Vertical</Radio.Button>
                                </Radio.Group>
                                <LinkToAnt componentName="form" />
                                {renderToggle()}
                            </>
                        }
                    >
                        <ComponentRow>
                            <Form layout={layout}>
                                <FormItem {...formItemLayout} label="Plain" hasFeedback>
                                    <Input size={size} placeholder="I'm the placeholder" />
                                </FormItem>{' '}
                                <FormItem
                                    {...formItemLayout}
                                    label="Success"
                                    hasFeedback
                                    validateStatus="success"
                                >
                                    <Input
                                        size={size}
                                        placeholder="I'm the placeholder"
                                        id="success"
                                    />
                                </FormItem>
                                <FormItem
                                    {...formItemLayout}
                                    label="Validating"
                                    hasFeedback
                                    validateStatus="validating"
                                    help="The information is being validated..."
                                >
                                    <Input
                                        size={size}
                                        placeholder="I'm the placeholder"
                                        id="validating"
                                    />
                                </FormItem>
                                <FormItem
                                    {...formItemLayout}
                                    label="Warning"
                                    validateStatus="warning"
                                    help="Hey you, I am a warning"
                                >
                                    <Input
                                        size={size}
                                        placeholder="I'm the placeholder"
                                        id="warning"
                                    />
                                </FormItem>
                                <FormItem
                                    {...formItemLayout}
                                    label="Error"
                                    hasFeedback
                                    validateStatus="error"
                                    help="Oh my, I am an error"
                                >
                                    <Input
                                        size={size}
                                        placeholder="I'm the placeholder"
                                        id="error"
                                    />
                                </FormItem>
                            </Form>
                        </ComponentRow>
                        {renderCode()}
                    </Card>
                )}
            </CodeExpand>
        );
    }
}
