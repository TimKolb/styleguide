import * as React from 'react';
import { Component } from 'react';
import { Button, Radio, Icon } from 'antd';
import { RadioChangeEvent } from 'antd/lib/radio/interface';
import { ButtonSize } from 'antd/lib/button/button';
import { ComponentRow } from '../components/ComponentRow';
import { Card } from '../components/Card';
import { LinkToAnt } from '../components/LinkToAnt';
import { CodeExpand } from '../composite/CodeExpand';

type State = {
    size: ButtonSize;
};

export class Buttons extends Component<{}, State> {
    state = {
        size: 'default' as ButtonSize,
    };

    handleSizeChange = (e: RadioChangeEvent) => {
        this.setState({ size: e.target.value });
    };

    public render() {
        const { size } = this.state;
        return (
            <CodeExpand code={require(`!raw-loader!${__filename}`)}>
                {(_, { renderToggle, renderCode }) => (
                    <Card
                        title="Buttons"
                        extra={
                            <>
                                <Radio.Group value={size} onChange={this.handleSizeChange}>
                                    <Radio.Button value="large">Large</Radio.Button>
                                    <Radio.Button value="default">Default</Radio.Button>
                                    <Radio.Button value="small">Small</Radio.Button>
                                </Radio.Group>
                                <LinkToAnt componentName="button" />
                                {renderToggle()}
                            </>
                        }
                    >
                        <ComponentRow>
                            <Button type="primary" size={size}>
                                Primary
                            </Button>
                            <Button size={size}>Normal</Button>
                            <Button type="dashed" size={size}>
                                Dashed
                            </Button>
                            <Button type="danger" size={size}>
                                Danger
                            </Button>
                        </ComponentRow>

                        <ComponentRow>
                            <Button type="primary" shape="circle" icon="download" size={size} />
                            <Button type="primary" icon="download" size={size}>
                                Download
                            </Button>
                            <Button.Group size={size}>
                                <Button type="primary">
                                    <Icon type="left" />Backward
                                </Button>
                                <Button type="primary">
                                    Forward<Icon type="right" />
                                </Button>
                            </Button.Group>
                        </ComponentRow>
                        {renderCode()}
                    </Card>
                )}
            </CodeExpand>
        );
    }
}
