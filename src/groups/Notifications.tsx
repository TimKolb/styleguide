import * as React from 'react';
import { Component } from 'react';
import { notification, Button } from 'antd';
import { CodeExpand } from '../composite/CodeExpand';
import { Card } from '../components/Card';
import { LinkToAnt } from '../components/LinkToAnt';
import { WithMarginClass } from './Messages';

const openNotificationWithIcon = (type: string) => {
    notification[type]({
        message: 'Notification Title',
        description:
            'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
    });
};

export class Notifications extends Component {
    render() {
        return (
            <CodeExpand code={require(`!raw-loader!${__filename}`)}>
                {(_, { renderToggle, renderCode }) => (
                    <Card
                        title="Notifications"
                        extra={
                            <>
                                <LinkToAnt componentName="notification" />
                                {renderToggle()}
                            </>
                        }
                    >
                        <div>
                            <Button
                                className={WithMarginClass}
                                onClick={openNotificationWithIcon.bind(null, 'success')}
                            >
                                Success
                            </Button>
                            <Button
                                className={WithMarginClass}
                                onClick={openNotificationWithIcon.bind(null, 'info')}
                            >
                                Info
                            </Button>
                            <Button
                                className={WithMarginClass}
                                onClick={openNotificationWithIcon.bind(null, 'warning')}
                            >
                                Warning
                            </Button>
                            <Button
                                className={WithMarginClass}
                                onClick={openNotificationWithIcon.bind(null, 'error')}
                            >
                                Error
                            </Button>
                        </div>
                        {renderCode()}
                    </Card>
                )}
            </CodeExpand>
        );
    }
}
