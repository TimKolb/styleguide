import * as React from 'react';
import { Steps, Button, message } from 'antd';
import { Component } from 'react';
import { LinkToAnt } from '../components/LinkToAnt';
import { Card } from '../components/Card';
import { CodeExpand } from '../composite/CodeExpand';

const Step = Steps.Step;

const steps = [
    {
        title: 'First',
        content: 'First-content',
    },
    {
        title: 'Second',
        content: 'Second-content',
    },
    {
        title: 'Last',
        content: 'Last-content',
    },
];

export class StepsDemo extends Component {
    state = {
        current: 0,
    };

    next = () => {
        const current = this.state.current + 1;
        this.setState({ current });
    };

    prev = () => {
        const current = this.state.current - 1;
        this.setState({ current });
    };

    handleSuccess = () => {
        message.success('Processing complete!');
    };

    render() {
        const { current } = this.state;
        return (
            <CodeExpand code={require(`!raw-loader!${__filename}`)}>
                {(_, { renderToggle, renderCode }) => (
                    <Card
                        title="Steps"
                        extra={
                            <>
                                <LinkToAnt componentName="steps" />
                                {renderToggle()}
                            </>
                        }
                    >
                        <div>
                            <Steps current={current}>
                                {steps.map(item => <Step key={item.title} title={item.title} />)}
                            </Steps>
                            <div>{steps[current].content}</div>
                            <div>
                                {current < steps.length - 1 && (
                                    <Button type="primary" onClick={this.next}>
                                        Next
                                    </Button>
                                )}
                                {current === steps.length - 1 && (
                                    <Button type="primary" onClick={this.handleSuccess}>
                                        Done
                                    </Button>
                                )}
                                {current > 0 && (
                                    <Button style={{ marginLeft: 8 }} onClick={this.prev}>
                                        Previous
                                    </Button>
                                )}
                            </div>
                        </div>
                        {renderCode()}
                    </Card>
                )}
            </CodeExpand>
        );
    }
}
