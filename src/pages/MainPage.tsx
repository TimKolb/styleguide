import * as React from 'react';
import { Component, Fragment } from 'react';
import 'antd/dist/antd.less';
import { Heading } from '../components/Heading';
import { BaseLayout } from '../layouts/BaseLayout';
import { Buttons } from '../groups/Buttons';
import { Inputs } from '../groups/Inputs';
import { AlternateInputs } from '../groups/AlternateInputs';
import { Alert, Row, Col } from 'antd';
import { css } from 'emotion';
import { cardHeadPadding } from '../theme';
import { ToolTips } from '../groups/ToolTips';
import { Messages } from '../groups/Messages';
import { Notifications } from '../groups/Notifications';
import { StepsDemo } from '../groups/Steps';
import { LoginDemo } from '../groups/LoginDemo';

const AlertClass = css`
    width: 100%;
    margin-top: ${cardHeadPadding};
`;

export class MainPage extends Component {
    private renderHeader = () => <Heading>Styleguide</Heading>;
    private renderSider = () => null;

    public render() {
        return (
            <BaseLayout renderHeader={this.renderHeader} renderSider={this.renderSider}>
                <Alert
                    type="info"
                    showIcon
                    className={AlertClass}
                    style={{ width: '100%', marginBottom: '1.5em' }}
                    message={
                        <Fragment>
                            Choose your components at{' '}
                            <a href="https://ant.design/components/progress/">ant.design</a>
                        </Fragment>
                    }
                />
                <Buttons />
                <Inputs />
                <AlternateInputs />
                <ToolTips />
                <Row gutter={16}>
                    <Col md={12}>
                        <Messages />
                    </Col>
                    <Col md={12}>
                        <Notifications />
                    </Col>
                </Row>

                <StepsDemo />
                <LoginDemo />
            </BaseLayout>
        );
    }
}
