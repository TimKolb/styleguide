import * as React from 'react';
import { Component, ReactNode } from 'react';
import Highlight from 'react-highlight';
import { Button, Divider } from 'antd';
import { Meta } from '../components/Card';
import { css } from 'emotion';
import { paddingSm } from '../theme';

type CodeExpandState = { expanded: boolean };
type CodeExpandActions = {
    handleToggle: () => void;
    renderToggle: () => ReactNode;
    renderCode: () => ReactNode;
};

export class CodeExpand extends Component<
    { code: string; children: (state: CodeExpandState, actions: CodeExpandActions) => ReactNode },
    CodeExpandState
> {
    state = {
        expanded: false,
    };

    handleToggle = () => this.setState(({ expanded }) => ({ expanded: !expanded }));

    renderToggle = () => (
        <Button
            className={css`
                margin-left: ${paddingSm};
            `}
            shape="circle"
            icon="code-o"
            onClick={this.handleToggle}
        />
    );

    renderCode = () =>
        this.state.expanded ? (
            <>
                <Divider dashed>Code</Divider>
                <Meta
                    description={
                        <Highlight className="typescript xml">{this.props.code}</Highlight>
                    }
                />
            </>
        ) : null;

    actions = {
        handleToggle: this.handleToggle,
        renderToggle: this.renderToggle,
        renderCode: this.renderCode,
    };

    render() {
        const { children } = this.props;
        return children(this.state, this.actions);
    }
}
