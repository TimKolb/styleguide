import * as React from 'react';
import { Formik, FormikConfig } from 'formik';
import { Form as AntdForm, Input as AntdInput, Button as AntdButton } from 'antd';
import { LoginProps, LoginValues } from './Login.types';
import { Component } from 'react';
import { ReactNode } from 'react';

const AntdFormItem = AntdForm.Item;

export const defaultComponents = {
    Form: AntdForm,
    FormItem: AntdFormItem,
    Input: AntdInput,
    Button: AntdButton,
};

export class Login<P = {}> extends Component<LoginProps<LoginValues & P>> {
    static defaultProps: Partial<LoginProps<LoginValues>> = {
        getPropsForEmailFormItem: () => null,
        getPropsForPasswordFormItem: () => null,
        getPropsForPasswordInput: () => null,
        getPropsForEmailInput: () => null,
        getPropsForButton: () => null,
        getPropsForForm: () => null,
        buttonChildren: 'Login',
        layout: 'horizontal',
        errorLabels: {
            email: {
                required: 'Required',
                invalid: 'Invalid email address',
            },
            password: {
                required: 'Required',
            },
        },
    };

    initialValue: LoginValues = {
        email: '',
        password: '',
    };

    validate: FormikConfig<LoginValues>['validate'] = values => {
        const { errorLabels } = this.props;
        const { email, password } = errorLabels!;
        const { required: requiredEmail, invalid: invalidEmail } = email;
        const { required: requiredPassword } = password;

        const errors: { email?: ReactNode; password?: ReactNode } = {};
        if (!values.email) {
            errors.email = requiredEmail;
        } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
            errors.email = invalidEmail;
        }

        if (!values.password) {
            errors.password = requiredPassword;
        }
        return errors;
    };

    handleSubmit: FormikConfig<LoginValues>['onSubmit'] = async (
        values: LoginValues,
        { setSubmitting, ...restSubmitOperation },
    ) => {
        const { onSubmit } = this.props;
        await onSubmit(values, restSubmitOperation);
        setSubmitting(false);
    };

    renderForm: FormikConfig<LoginValues>['children'] = ({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
    }) => {
        const {
            components,
            getPropsForButton,
            getPropsForEmailFormItem,
            getPropsForEmailInput,
            getPropsForForm,
            getPropsForPasswordFormItem,
            getPropsForPasswordInput,
            buttonChildren,
            layout,
        } = this.props;
        const { Form, FormItem, Input, Button } = { ...defaultComponents, ...components } as any;

        const getFormItemProps = (key: 'email' | 'password') => {
            const showError = !!touched[key] && !!errors[key];
            return {
                help: showError ? errors[key] : null,
                validateStatus: (() => {
                    if (showError) {
                        return 'error';
                    }
                    if (!errors[key]) {
                        return 'success';
                    }
                    return undefined;
                })(),
                formItemLayout:
                    layout === 'horizontal'
                        ? {
                              labelCol: { span: 8 },
                              wrapperCol: { span: 16 },
                          }
                        : null,
                ...(key === 'email' ? getPropsForEmailFormItem!() : getPropsForPasswordFormItem!()),
            };
        };

        return (
            <Form {...getPropsForForm!()} onSubmit={handleSubmit}>
                <FormItem label="Email" {...getFormItemProps('email')}>
                    <Input
                        type="email"
                        name="email"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.email}
                        {...getPropsForEmailInput!()}
                    />
                </FormItem>
                <FormItem label="Password" {...getFormItemProps('password')}>
                    <Input
                        type="password"
                        name="password"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.password}
                        {...getPropsForPasswordInput!()}
                    />
                </FormItem>
                <Button
                    type="primary"
                    htmlType="submit"
                    loading={isSubmitting}
                    {...getPropsForButton!()}
                >
                    {buttonChildren}
                </Button>
            </Form>
        );
    };

    render() {
        const { children, formProps } = this.props;
        return (
            <Formik
                initialValues={this.initialValue}
                validate={this.validate}
                onSubmit={this.handleSubmit}
                {...formProps}
            >
                {typeof children === 'function' ? children : this.renderForm}
            </Formik>
        );
    }
}
