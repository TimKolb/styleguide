import { FormikConfig } from 'formik';
import { FormLayout } from 'antd/lib/form/Form';
import { ReactNode } from 'react';

export interface LoginProps<Values = {}> {
    onSubmit(values: object, submitOperations: object): Promise<void> | void;
    getPropsForEmailFormItem?(): object | null;
    getPropsForPasswordFormItem?(): object | null;
    getPropsForPasswordInput?(): object | null;
    getPropsForEmailInput?(): object | null;
    getPropsForButton?(): object | null;
    getPropsForForm?(): object | null;
    buttonChildren?: string;
    layout?: FormLayout;
    children?: FormikConfig<LoginValues & Values>['render'];
    components?: {
        Form?: any;
        FormItem?: any;
        Input?: any;
        Button?: any;
    };
    formProps?: FormikConfig<LoginValues & Values>;
    errorLabels?: {
        email: {
            required: ReactNode;
            invalid: ReactNode;
        };
        password: {
            required: ReactNode;
        };
    };
}

export interface LoginValues {
    email: string;
    password: string;
}
