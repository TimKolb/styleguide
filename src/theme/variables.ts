import { rgba, tint, hsl } from 'polished';
import { colorPalette } from './color/colorPalette';
import { blue6, gold6, green6, red6, yellow6 } from './color/colors';

// The prefix to use on all css classes from ant.
export const antPrefix = 'ant';

// -------- Colors -----------
export const primaryColor = blue6;
export const infoColor = blue6;
export const successColor = green6;
export const processingColor = blue6;
export const errorColor = red6;
export const highlightColor = red6;
export const warningColor = gold6;
export const normalColor = '#d9d9d9';

// Color used by default to control hover and active backgrounds and for
// alert info backgrounds.
export const primary1 = colorPalette(primaryColor, 1); // replace tint(primaryColor, 0.90)
export const primary2 = colorPalette(primaryColor, 2); // replace tint(primaryColor, 0.80)
export const primary3 = colorPalette(primaryColor, 3); // unused
export const primary4 = colorPalette(primaryColor, 4); // unused
export const primary5 = colorPalette(primaryColor, 5); // color used to control the text color in many active and hover states, replace tint(primaryColor, 0.20)
export const primary6 = primaryColor; // color used to control the text color of active buttons, don't use, use primaryColor
export const primary7 = colorPalette(primaryColor, 7); // replace shade(primaryColor, 0.5)
export const primary8 = colorPalette(primaryColor, 8); // unused
export const primary9 = colorPalette(primaryColor, 9); // unused
export const primary10 = colorPalette(primaryColor, 10); // unused

// Base Scaffolding Variables
// ---

// Background color for `<body>`
export const bodyBackground = '#fff';
// Base background color for most components
export const componentBackground = '#fff';

// Font

export const googleFonts = [
    { family: 'Source Sans Pro' },
    { family: 'Wendy One' },
    { family: 'Roboto', variants: ['400', '700italic'] },
];

export const fontFamilyNoNumber = `Source Sans Pro, AppleSystem, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Helvetica, Arial, sansSerif`;
export const fontFamily = `"Monospaced Number", ${fontFamilyNoNumber}`;
export const codeFamily = 'Consolas, Menlo, Courier, monospace';
export const headingColor = rgba('#000', 0.85);
export const textColor = rgba('#000', 0.65);
export const textColorSecondary = rgba('#000', 0.45);
export const headingColorDark = rgba('#fff', 0.1);
export const textColorDark = rgba('#fff', 0.85);
export const textColorSecondaryDark = rgba('#fff', 0.65);
export const fontSizeBase = '14px';
export const fontSizeLg = `${fontSizeBase} + 2px`;
export const fontSizeSm = '12px';
export const lineHeightBase = 1.5;
export const borderRadiusBase = '4px';
export const borderRadiusSm = '2px';

// vertical paddings
export const paddingLg = '24px'; // containers
export const paddingMd = '16px'; // small containers and buttons
export const paddingSm = '12px'; // Form controls and items
export const paddingXs = '8px'; // small items

// vertical padding for all form controls
export const controlPaddingHorizontal = paddingSm;
export const controlPaddingHorizontalSm = paddingXs;

// The background colors for active and hover states for things like
// list items or table cells.
export const itemActiveBg = primary1;
export const itemHoverBg = primary1;

// ICONFONT
export const iconfontCssPrefix = 'anticon';
export const iconUrl = 'https=//at.alicdn.com/t/font_148784_v4ggb6wrjmkotj4i';

// LINK
export const linkColor = primaryColor;
export const linkHoverColor = colorPalette(linkColor, 5);
export const linkActiveColor = colorPalette(linkColor, 7);
export const linkDecoration = 'none';
export const linkHoverDecoration = 'none';

// Animation
export const easeOut = 'cubic-bezier(0.215, 0.61, 0.355, 1);';
export const easeIn = 'cubic-bezier(0.55, 0.055, 0.675, 0.19);';
export const easeInOut = 'cubic-bezier(0.645, 0.045, 0.355, 1);';
export const easeOutBack = 'cubic-bezier(0.12, 0.4, 0.29, 1.46);';
export const easeInBack = 'cubic-bezier(0.71, 0.46, 0.88, 0.6);';
export const easeInOutBack = 'cubic-bezier(0.71, 0.46, 0.29, 1.46);';
export const easeOutCirc = 'cubic-bezier(0.08, 0.82, 0.17, 1);';
export const easeInCirc = 'cubic-bezier(0.6, 0.04, 0.98, 0.34);';
export const easeInOutCirc = 'cubic-bezier(0.78, 0.14, 0.15, 0.86);';
export const easeOutQuint = 'cubic-bezier(0.23, 1, 0.32, 1);';
export const easeInQuint = 'cubic-bezier(0.755, 0.05, 0.855, 0.06);';
export const easeInOutQuint = 'cubic-bezier(0.86, 0, 0.07, 1);';

// Border color
export const borderColorBase = hsl(0, 0, 0.85); // base border outline a component
export const borderColorSplit = hsl(0, 0, 0.91); // split border inside a component
export const borderWidthBase = '1px'; // width of the border for a component
export const borderStyleBase = 'solid'; // style of a components border

// Outline
export const outlineBlurSize = 0;
export const outlineWidth = '2px';
export const outlineColor = primaryColor;

export const backgroundColorLight = hsl(0, 0, 0.98); // background of header and selected item
export const backgroundColorBase = hsl(0, 0, 0.96); // Default grey background color

// Disabled states
export const disabledColor = rgba('#000', 0.25);
export const disabledBg = backgroundColorBase;
export const disabledColorDark = rgba('#fff', 0.35);

// Shadow
export const shadowColor = rgba(0, 0, 0, 0.15);
export const shadow1Up = `0 2px 8px ${shadowColor}`;
export const shadow1Down = `0 2px 8px ${shadowColor}`;
export const shadow1Left = `2px 0 8px ${shadowColor}`;
export const shadow1Right = `2px 0 8px ${shadowColor}`;
export const shadow2 = `0 4px 12px ${shadowColor}`;
export const boxShadowBase = shadow1Down;

// Buttons
export const btnFontWeight = 400;
export const btnBorderRadiusBase = borderRadiusBase;
export const btnBorderRadiusSm = borderRadiusBase;

export const btnPrimaryColor = '#fff';
export const btnPrimaryBg = primaryColor;

export const btnDefaultColor = textColor;
export const btnDefaultBg = '#fff';
export const btnDefaultBorder = borderColorBase;

export const btnDangerColor = errorColor;
export const btnDangerBg = backgroundColorBase;
export const btnDangerBorder = borderColorBase;

export const btnDisableColor = disabledColor;
export const btnDisableBg = disabledBg;
export const btnDisableBorder = borderColorBase;

export const btnPaddingBase = `0 ${paddingMd} - 1px`;
export const btnFontSizeLg = fontSizeLg;
export const btnFontSizeSm = fontSizeBase;
export const btnPaddingLg = btnPaddingBase;
export const btnPaddingSm = `0 ${paddingXs} - 1px`;

export const btnHeightBase = '32px';
export const btnHeightLg = '40px';
export const btnHeightSm = '24px';

export const btnCircleSize = btnHeightBase;
export const btnCircleSizeLg = btnHeightLg;
export const btnCircleSizeSm = btnHeightSm;

export const btnGroupBorder = primary5;

// Checkbox
export const checkboxSize = '16px';
export const checkboxColor = primaryColor;

// Radio
export const radioSize = '16px';
export const radioDotColor = primaryColor;

// Radio buttons
export const radioButtonBg = btnDefaultBg;
export const radioButtonColor = btnDefaultColor;
export const radioButtonHoverColor = primary5;
export const radioButtonActiveColor = primary7;

// Media queries breakpoints
// Extra small screen / phone
export const screenXs = '480px';
export const screenXsMin = screenXs;

// Small screen / tablet
export const screenSm = '576px';
export const screenSmMin = screenSm;

// Medium screen / desktop
export const screenMd = '768px';
export const screenMdMin = screenMd;

// Large screen / wide desktop
export const screenLg = '992px';
export const screenLgMin = screenLg;

// Extra large screen / full hd
export const screenXl = '1200px';
export const screenXlMin = screenXl;

// Extra extra large screen / large descktop
export const screenXxl = '1600px';
export const screenXxlMin = screenXxl;

// provide a maximum
export const screenXsMax = `(${screenSmMin} - 1px)`;
export const screenSmMax = `(${screenMdMin} - 1px)`;
export const screenMdMax = `(${screenLgMin} - 1px)`;
export const screenLgMax = `(${screenXlMin} - 1px)`;
export const screenXlMax = `(${screenXxlMin} - 1px)`;

// Grid system
export const gridColumns = 24;
export const gridGutterWidth = 0;

// Layout
export const layoutBodyBackground = '#f0f2f5';
export const layoutHeaderBackground = '#001529';
export const layoutFooterBackground = layoutBodyBackground;
export const layoutHeaderHeight = '64px';
export const layoutHeaderPadding = '0 50px';
export const layoutFooterPadding = '24px 50px';
export const layoutSiderBackground = layoutHeaderBackground;
export const layoutTriggerHeight = '48px';
export const layoutTriggerBackground = '#002140';
export const layoutTriggerColor = '#fff';
export const layoutZeroTriggerWidth = '36px';
export const layoutZeroTriggerHeight = '42px';
// Layout light theme
export const layoutSiderBackgroundLight = '#fff';
export const layoutTriggerBackgroundLight = '#fff';
export const layoutTriggerColorLight = textColor;

// zIndex list
export const zindexAffix = 10;
export const zindexBackTop = 10;
export const zindexModalMask = 1000;
export const zindexModal = 1000;
export const zindexNotification = 1010;
export const zindexMessage = 1010;
export const zindexPopover = 1030;
export const zindexPicker = 1050;
export const zindexDropdown = 1050;
export const zindexTooltip = 1060;

// Animation
export const animationDurationSlow = '.3s'; // Modal
export const animationDurationBase = '.2s';
export const animationDurationFast = '.1s'; // Tooltip

// Form
// ---
export const labelRequiredColor = highlightColor;
export const labelColor = headingColor;
export const formItemMarginBottom = '24px';
export const formItemTrailingColon = true;
export const formVerticalLabelPadding = '0 0 8px';
export const formVerticalLabelMargin = 0;

// Input
// ---
export const inputHeightBase = '32px';
export const inputHeightLg = '40px';
export const inputHeightSm = '24px';
export const inputPaddingHorizontal = `${controlPaddingHorizontal}  - 1px`;
export const inputPaddingHorizontalBase = inputPaddingHorizontal;
export const inputPaddingHorizontalSm = `${controlPaddingHorizontalSm}  - 1px`;
export const inputPaddingHorizontalLg = `${inputPaddingHorizontal} `;
export const inputPaddingVerticalBase = '4px';
export const inputPaddingVerticalSm = '1px';
export const inputPaddingVerticalLg = '6px';
export const inputPlaceholderColor = hsl(0, 0, 0.75);
export const inputColor = textColor;
export const inputBorderColor = borderColorBase;
export const inputBg = '#fff';
export const inputAddonBg = backgroundColorLight;
export const inputHoverBorderColor = primaryColor;
export const inputDisabledBg = disabledBg;

// Tooltip
// ---
//* Tooltip max width
export const tooltipMaxWidth = '250px';
//** Tooltip text color
export const tooltipColor = '#fff';
//** Tooltip background color
export const tooltipBg = rgba(0, 0, 0, 0.75);
//** Tooltip arrow width
export const tooltipArrowWidth = '5px';
//** Tooltip distance with trigger
export const tooltipDistance = `${tooltipArrowWidth}  - 1px + 4px`;
//** Tooltip arrow color
export const tooltipArrowColor = tooltipBg;

// Popover
// ---
//** Popover body background color
export const popoverBg = '#fff';
//** Popover text color
export const popoverColor = textColor;
//** Popover maximum width
export const popoverMinWidth = '177px';
//** Popover arrow width
export const popoverArrowWidth = '6px';
//** Popover arrow color
export const popoverArrowColor = popoverBg;
//** Popover outer arrow width
//** Popover outer arrow color
export const popoverArrowOuterColor = popoverBg;
//** Popover distance with trigger
export const popoverDistance = `${popoverArrowWidth}  + 4px`;

// Modal
// --
export const modalMaskBg = rgba(0, 0, 0, 0.65);

// Progress
// --
export const progressDefaultColor = processingColor;
export const progressRemainingColor = backgroundColorBase;

// Menu
// ---
export const menuInlineToplevelItemHeight = '40px';
export const menuItemHeight = '40px';
export const menuCollapsedWidth = '80px';
export const menuBg = componentBackground;
export const menuItemColor = textColor;
export const menuHighlightColor = primaryColor;
export const menuItemActiveBg = itemActiveBg;
export const menuItemGroupTitleColor = textColorSecondary;
// dark theme
export const menuDarkColor = textColorSecondaryDark;
export const menuDarkBg = layoutHeaderBackground;
export const menuDarkArrowColor = '#fff';
export const menuDarkSubmenuBg = '#000c17';
export const menuDarkHighlightColor = '#fff';
export const menuDarkItemActiveBg = primaryColor;

// Spin
// ---
export const spinDotSizeSm = '14px';
export const spinDotSize = '20px';
export const spinDotSizeLg = '32px';

// Table
// --
export const tableHeaderBg = backgroundColorLight;
export const tableHeaderSortBg = backgroundColorBase;
export const tableRowHoverBg = primary1;
export const tableSelectedRowBg = '#fafafa';
export const tableExpandedRowBg = '#fbfbfb';
export const tablePaddingVertical = '16px';
export const tablePaddingHorizontal = '16px';

// Tag
// --
export const tagDefaultBg = backgroundColorLight;
export const tagDefaultColor = textColor;
export const tagFontSize = fontSizeSm;

// TimePicker
// ---
export const timePickerPanelColumnWidth = '56px';
export const timePickerPanelWidth = `${timePickerPanelColumnWidth} * 3`;
export const timePickerSelectedBg = backgroundColorBase;

// Carousel
// ---
export const carouselDotWidth = '16px';
export const carouselDotHeight = '3px';
export const carouselDotActiveWidth = '24px';

// Badge
// ---
export const badgeHeight = '20px';
export const badgeDotSize = '6px';
export const badgeFontSize = fontSizeSm;
export const badgeFontWeight = 'normal';
export const badgeStatusSize = '6px';

// Rate
// ---
export const rateStarColor = yellow6;
export const rateStarBg = borderColorSplit;

// Card
// ---
export const cardHeadColor = headingColor;
export const cardHeadBackground = componentBackground;
export const cardHeadPadding = '16px';
export const cardInnerHeadPadding = '12px';
export const cardPaddingBase = '24px';
export const cardPaddingWider = '32px';
export const cardActionsBackground = backgroundColorLight;
export const cardShadow = '0 2px 8px rgba(0, 0, 0, 0.9)';

// Tabs
// ---
export const tabsCardHeadBackground = backgroundColorLight;
export const tabsCardHeight = '40px';
export const tabsCardActiveColor = primaryColor;
export const tabsTitleFontSize = fontSizeBase;
export const tabsTitleFontSizeLg = fontSizeLg;
export const tabsTitleFontSizeSm = fontSizeBase;
export const tabsInkBarColor = primaryColor;
export const tabsBarMargin = '0 0 16px 0';
export const tabsHorizontalMargin = '0 32px 0 0';
export const tabsHorizontalPadding = '12px 16px';
export const tabsVerticalPadding = '8px 24px';
export const tabsVerticalMargin = '0 0 16px 0';
export const tabsScrollingSize = '32px';
export const tabsHighlightColor = primaryColor;
export const tabsHoverColor = primary5;
export const tabsActiveColor = primary7;

// BackTop
// ---
export const backTopColor = '#fff';
export const backTopBg = textColorSecondary;
export const backTopHoverBg = textColor;

// Avatar
// ---
export const avatarSizeBase = '32px';
export const avatarSizeLg = '40px';
export const avatarSizeSm = '24px';
export const avatarFontSizeBase = '18px';
export const avatarFontSizeLg = '24px';
export const avatarFontSizeSm = '14px';
export const avatarBg = '#ccc';
export const avatarColor = '#fff';
export const avatarBorderRadius = borderRadiusBase;

// Switch
// ---
export const switchHeight = '22px';
export const switchSmHeight = '16px';
export const switchSmCheckedMarginLeft = `-(${switchSmHeight} - 3px)`;
export const switchDisabledOpacity = 0.4;
export const switchColor = primaryColor;

// Pagination
// ---
export const paginationItemSize = '32px';
export const paginationItemSizeSm = '24px';
export const paginationFontFamily = 'Arial';
export const paginationFontWeightActive = 500;

// Breadcrumb
// ---
export const breadcrumbBaseColor = textColorSecondary;
export const breadcrumbLastItemColor = textColor;
export const breadcrumbFontSize = fontSizeBase;
export const breadcrumbIconFontSize = fontSizeSm;
export const breadcrumbLinkColor = textColorSecondary;
export const breadcrumbLinkColorHover = primary5;
export const breadcrumbSeparatorColor = textColorSecondary;
export const breadcrumbSeparatorMargin = '0 paddingXs';

// Slider
// ---
export const sliderMargin = '14px 6px 10px';
export const sliderRailBackgroundColor = backgroundColorBase;
export const sliderRailBackgroundColorHover = '#e1e1e1';
export const sliderTrackBackgroundColor = primary3;
export const sliderTrackBackgroundColorHover = primary4;
export const sliderHandleColor = primary3;
export const sliderHandleColorHover = primary4;
export const sliderHandleColorFocus = tint(0.2, primaryColor);
export const sliderHandleColorFocusShadow = tint(0.5, primaryColor);
export const sliderHandleColorTooltipOpen = primaryColor;
export const sliderDotBorderColor = borderColorSplit;
export const sliderDotBorderColorActive = tint(0.5, primaryColor);
export const sliderDisabledColor = disabledColor;
export const sliderDisabledBackgroundColor = componentBackground;

// Collapse
// ---
export const collapseHeaderPadding = '12px 0 12px 40px';
export const collapseHeaderBg = backgroundColorLight;
export const collapseContentPadding = paddingMd;
export const collapseContentBg = componentBackground;
