import { fontFamilyNoNumber } from './variables';

export const fontFamilyFancy = `"Wendy One", ${fontFamilyNoNumber}`;
