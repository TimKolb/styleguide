import * as variables from './variables';
export * from './variables';
import { paramCase } from 'change-case';

const keyBlackList = ['googleFonts'];

export const theme = Object.entries(variables).reduce((themeVars, [name, value]) => {
    if (name in keyBlackList) {
        return themeVars;
    }
    return {
        ...themeVars,
        [`@${paramCase(addNumberDash(name))}`]: value,
    };
}, {});

function addNumberDash(target: string) {
    if (target.search(/\d+/) > 0) {
        return target.replace(/(\d+)/, '-$1');
    }
    return target;
}
