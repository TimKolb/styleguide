import { theme } from '.';

it('has no undefined less variables', () => {
    Object.entries(theme).forEach(([key, value]) => expect(value).toBeDefined());
});
