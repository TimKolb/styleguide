import * as React from 'react';
import { Component } from 'react';
import 'antd/dist/antd.less';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { MainPage } from './pages/MainPage';

export class App extends Component {
    public render() {
        return (
            <Router>
                <Route exact path={process.env.PUBLIC_URL} component={MainPage} />
            </Router>
        );
    }
}