Card example:

```jsx
<Card title="Card title" extra={<span>Extra</span>}>
    <span>Lorizzle doggy cool sit shiz, dope adipiscing bow wow wow. Nullizzle sheezy velit, dawg volutpizzle, rizzle go to hizzle, brizzle vel, arcu. Pellentesque eget tellivizzle. Sizzle erizzle. That's the shizzle at dolor fizzle i'm in the shizzle tempus tellivizzle. Maurizzle bling bling ass izzle black. Pot sure tortor. Fo shizzle mah nizzle fo rizzle, mah home g-dizzle eleifend rhoncizzle fizzle. In sizzle habitasse platea dictumst. Dawg dapibizzle. Curabitur tellus urna, pretizzle eu, mattizzle ac, eleifend vitae, nunc. Sizzle suscipizzle. Integizzle sempizzle velit cool purus.</span>
</Card>
```
