import { Button } from 'antd';
import * as React from 'react';
import { paddingMd } from '../theme';
import styled from 'react-emotion';
import 'antd/lib/button/style';

const StyledButtonLink = styled(Button)`
    margin-left: ${paddingMd};
`;

export const LinkToAnt = ({ componentName }: { componentName: string }) => (
    <StyledButtonLink
        href={`https://ant.design/components/${componentName}/`}
        target="_blank"
        rel="noopener nofollow"
        shape="circle"
        icon="ant-design"
    />
);
