import styled from 'react-emotion';

export const ComponentRow = styled('div')`
    width: auto;
    display: flex;
    justify-content: flex-start;
    flex-wrap: wrap;
    & ~ & {
        margin-top: 1em;
    }

    > * {
        margin-right: 1em;
    }
`;
