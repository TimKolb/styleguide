import styled from 'react-emotion';
import { textColorDark } from '../theme';
import 'antd/lib/style';

export const Heading = styled('h1')`
    color: ${textColorDark};
    font-size: 2em;
`;
