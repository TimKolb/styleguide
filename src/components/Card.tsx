import styled from 'react-emotion';
import { Card as AntCard } from 'antd';
import 'antd/lib/card/style';

export const Card = styled(AntCard)`
    margin-top: 1em;
`;

export const Meta = AntCard.Meta;
