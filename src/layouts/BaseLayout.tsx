import { Layout as AntdLayout } from 'antd';
import * as React from 'react';
const { Header, Sider, Content } = AntdLayout;
import 'antd/dist/antd.less';
import styled from 'react-emotion';
import { injectGlobal } from 'emotion';
import { ReactNode } from 'react';
import { screenXl } from '../theme';

injectGlobal`
    html, body, #root {
        height: 100%;
  }
`;

const MainLayout = styled(AntdLayout)`
    min-height: 100vh;
`;

const MainContent = styled(Content)`
    height: 100%;
    width: 100%;
    max-width: ${screenXl};
    margin: 0 auto;
`;

const InnerContent = styled('div')`
    padding: 24px;
    min-height: 100%;
`;

type BaseLayoutProps = Partial<{
    renderSider?: RenderCallback;
    renderHeader?: RenderCallback;
}>;

type RenderCallback = () => ReactNode | null;

export class BaseLayout extends React.Component<BaseLayoutProps, {}> {
    public render() {
        const { renderSider, renderHeader, children } = this.props;

        const sider = renderSider ? renderSider() : null;
        const header = renderHeader ? renderHeader() : null;

        return (
            <MainLayout>
                {sider && (
                    <Sider breakpoint="md" collapsedWidth="0">
                        {sider}
                    </Sider>
                )}
                <AntdLayout>
                    {header && <Header>{header}</Header>}
                    <MainContent>
                        <InnerContent>{children}</InnerContent>
                    </MainContent>
                </AntdLayout>
            </MainLayout>
        );
    }
}
