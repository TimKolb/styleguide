require('ts-node/register');
const path = require('path');
const { injectBabelPlugin } = require('react-app-rewired');
const rewireLess = require('react-app-rewire-less');
const GoogleFontsPlugin = require('google-fonts-webpack-plugin');

function addGoogleFonts(fonts, config) {
    return {
        ...config,
        plugins: [...config.plugins, new GoogleFontsPlugin({ fonts })],
    };
}

function addCacheLoader(config) {
    return {
        ...config,
        module: {
            ...config.module,
            rules: [
                ...config.module.rules,
                {
                    test: /\.(js|jsx|mjs|ts|tsx|.css|.less)$/,
                    use: ['cache-loader'],
                    include: path.resolve('src'),
                },
            ],
        },
    };
}

module.exports = function override(config, env) {
    const { theme, googleFonts } = require('./src/theme/index');

    config = injectBabelPlugin(['import', { libraryName: 'antd', style: true }], config); // change importing css to less
    config = rewireLess.withLoaderOptions({
        javascriptEnabled: true,
        modifyVars: theme,
    })(config, env);
    config = addGoogleFonts(googleFonts, config);
    config = addCacheLoader(config);
    return config;
};
